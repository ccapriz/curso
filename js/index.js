     document.addEventListener("DOMContentLoaded", function(){
        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
        var tooltipList = tooltipTriggerList.map(function(element){
            return new bootstrap.Tooltip(element);
        });
    });

    

    
    document.addEventListener("DOMContentLoaded", function(){
        var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
        var popoverList = popoverTriggerList.map(function(element){
        return new bootstrap.Popover(element);
        });
    });
   
    

   
      $(function(){
            $('.carousel').carousel({
              interval:2000
               });
            $('#contacto').on('show.bs.modal', function (e){
              console.log('el modal contacto se está mostrando');

              $('#contactoBtn').removeClass('btn-outline-success');
              $('#contactoBtn').addClass('btn-primary');
              $('#contactoBtn').prop('disabled',true);
            });
            $('#contacto').on('shown.bs.modal', function (e){
                console.log('el modal contacto se mostró');
            });
            $('#contacto').on('hide.bs.modal', function (e){
                console.log('el modal contacto se oculta');
            });
            $('#contacto').on('hidden.bs.modal', function (e){
                console.log('el modal contacto se ocultó');
                $('#contactoBtn').prop('disabled',false);
            });
      });
    